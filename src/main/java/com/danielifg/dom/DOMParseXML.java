package com.danielifg.dom;

import com.danielifg.bean.Movie;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.enterprise.context.RequestScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 *
 * @author Daniel
 *
 * CLass to Load the XML Document, Parse for the Queries by title, actor or
 * director, and loaded into the Movie been.
 *
 *
 */
@RequestScoped
public class DOMParseXML {

    private Document doc = null;

    private String xmlFile = null;
    private String xsdFile = null;
    private String message;
    private final Movie movie;

    public DOMParseXML(Movie movie, String xmlFile, String xsdFile) {
        this.xmlFile = xmlFile;
        this.movie = movie;
        this.xsdFile = xsdFile;

    }

    public boolean readDocument() {

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            db = dbf.newDocumentBuilder();

            doc = db.parse(new File(xmlFile));
            message = "true";
            return true;

        } catch (ParserConfigurationException ex) {
            message = "\nParserConfiguration error because ";
            message = ex.getMessage();
        } catch (SAXException ex) {
            message = "xml" + " cannot be parsed because ";
            message = ex.getMessage();
        } catch (IOException ex) {
            message = "\nI/O Exception because";
            message = ex.getMessage();
        }
        return false;
    }

    public boolean validSchema() {
        XMLSchemaValidator valid = new XMLSchemaValidator(xmlFile, xsdFile, message);
        return valid.validate();
    }

    /**
     * Load the XML query results into the Movie bean.
     *
     * @param movie
     * @param eElement
     * @return
     *
     */
    public Movie loadBean(Element eElement) {

        movie.setTitle(eElement.getElementsByTagName("title").item(0).getTextContent());
        movie.setRate(eElement.getElementsByTagName("rate").item(0).getTextContent());

        String[] split = eElement.getElementsByTagName("show_times").item(0).getTextContent().split("\\s+");
        movie.getTimesList().addAll(Arrays.asList(split));

        movie.setReleaseDate(eElement.getElementsByTagName("release_date").item(0).getTextContent());
        movie.setRunTime(eElement.getElementsByTagName("run_time").item(0).getTextContent());
        movie.setGenre(eElement.getElementsByTagName("genre").item(0).getTextContent());

        String actors = (eElement.getElementsByTagName("actors").item(0).getTextContent());
        movie.getActorsList().addAll(Arrays.asList(actors.split("\\s{2,2}")));

        String directors = eElement.getElementsByTagName("directors").item(0).getTextContent();
        movie.getDirectorsList().addAll(Arrays.asList(directors.split("\\s{2,2}")));

        String producers = eElement.getElementsByTagName("producers").item(0).getTextContent();
        movie.getProducersList().addAll(Arrays.asList(producers.split("\\s{2,2}")));

        String writers = eElement.getElementsByTagName("writers").item(0).getTextContent();
        movie.getWritersList().addAll(Arrays.asList(writers.split("\\s{2,2}")));

        movie.setStudio(eElement.getElementsByTagName("studio").item(0).getTextContent());
        movie.setImage(eElement.getElementsByTagName("image").item(0).getTextContent());

        return movie;
    }

    public String getMessage() {
        return message;
    }
    /**
     * Loop through the Document to find query
     *
     * @param title
     *
     * @return if title in Movies file.
     */
    
    public boolean findMovieByTitle(String title) {
        if (validSchema()) {
            readDocument();
            NodeList nList = doc.getElementsByTagName("movie");
            System.out.println(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                Element eElement = (Element) nNode;
                if (eElement.getElementsByTagName("title").item(0).getTextContent().equalsIgnoreCase(title)) {
                    loadBean(eElement).toString();
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Loop through the Document to find query
     *
     * @param actor
     * @return if Actor in Movies file.
     */
    public boolean findMovieByActorName(String actor) {

        readDocument();
        NodeList nList = doc.getElementsByTagName("movie");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;
            NodeList ns = (NodeList) eElement.getElementsByTagName("actor");
            for (int i = 0; i < ns.getLength(); i++) {
                if (ns.item(i).getTextContent().equalsIgnoreCase(actor)) {
                    loadBean(eElement);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Loop through the Document to find query
     *
     * @param director
     * @return if director in Movies file.
     */
    public boolean findMovieByDirectorName(String director) {
        readDocument();
        NodeList nList = doc.getElementsByTagName("movie");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;
            NodeList ns = (NodeList) eElement.getElementsByTagName("director");
            for (int i = 0; i < ns.getLength(); i++) {
                if (ns.item(i).getTextContent().equalsIgnoreCase(director)) {
                    loadBean(eElement);
                    return true;
                }
            }
        }
        return false;
    }

//    public static void main(String[] args) {
//        Movie mo = new Movie();
//        String xml = "movies.xml";
//        DOMParseXML dom = new DOMParseXML(xml, mo);
//        dom.findMovieByActorName("Joey Lauren Adams");
//        System.out.println(dom.movie.toString());
//
//    }
}
