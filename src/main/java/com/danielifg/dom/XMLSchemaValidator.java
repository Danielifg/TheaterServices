/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifg.dom;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

/**
 *
 * @author Daniel
 *
 * Class to validate the XML document before loading it for reading. Routine
 * courtesy of Kenneth Fogel - Concordia University
 *
 */
public class XMLSchemaValidator {

    /**
     *
     * @param xmlName
     * @param xsdName
     * @param xmlString
     * @return boolean for a valid XML document.
     */
    private String xmlFile = null;
    private String xsdFile = null;
    private String builder;

    public XMLSchemaValidator(String xmlFile, String xsdFile, String message) {
        this.xmlFile = xmlFile;
        this.xsdFile = xsdFile;
        this.builder = message;

    }
    public String getBuilder(){
        return this.builder;
    }

    public boolean validate() {

        boolean validXML = true;

        try {
            // 1. Lookup a factory for the W3C XML Schema language
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // 2. Compile the schema.
            // Here the schema is loaded from a java.io.File, but you could use
            // a java.net.URL or a javax.xml.transform.Source instead.
            File schemaLocation = new File(xsdFile);
            Schema schema = factory.newSchema(schemaLocation);

            // 3. Get a validator from the schema.
            Validator validator = schema.newValidator();

            // 4. Parse the document you want to check.
            Source source = new StreamSource(xmlFile);

            // 5. Check the document
            validator.validate(source);
            System.out.println(xmlFile + " is valid.");
        } catch (SAXException ex) {
            builder= "\n"+ xmlFile +" is not valid because";
            builder="\n"+ex.getMessage();
            validXML = false;
        } catch (IOException e) {
            builder="\nI/O Exception";
            builder="\n"+e.getMessage();
            validXML = false;
        }
        return validXML;
    }

}
