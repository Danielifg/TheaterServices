/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifg.sax;

import com.danielifg.bean.Movie;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Daniel
 */
@RequestScoped
public class SAXParseXML extends DefaultHandler implements Serializable {

    private final Movie movie;
    private Movie movie2;
    private String xmlFile = null;
    private String message = null;
    private final List<Movie> movieList;
    private String value;

    public SAXParseXML(String xmlFile, Movie movie) {
        this.movie = movie;
        this.xmlFile = xmlFile;
        movieList = new ArrayList<>();
    }

    public void parseDocument() {
        // parse
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(new File(xmlFile), this);
        } catch (ParserConfigurationException e) {
            message = ("\nParserConfig error");
        } catch (SAXException e) {
            message = ("\nSAXException : xml not well formed");
        } catch (IOException e) {
            message = ("\nIO error");
        }
    }

    @Override
    public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {
        // Read if current element has attributes ie: <movie id="001">
        // String i = attributes.getValue("name"); 

        if (elementName.equalsIgnoreCase("movie")) {
            movie2 = new Movie();
        }

    }

    @Override
    public void endElement(String s, String s1, String element) throws SAXException {

        if (element.equals("movie")) {
            movieList.add(movie2);
        }
        if (element.equalsIgnoreCase("title")) {
            movie2.setTitle(value);
        }
        if (element.equalsIgnoreCase("rate")) {
            movie2.setRate(value);
        }
        if (element.equalsIgnoreCase("show_time")) {
            movie2.getTimesList().add(value);
        }
        if (element.equalsIgnoreCase("release_date")) {
            movie2.setReleaseDate(value);
        }
        if (element.equalsIgnoreCase("run_time")) {
            movie2.setRunTime(value);
        }
        if (element.equalsIgnoreCase("genre")) {
            movie2.setGenre(value);
        }
        if (element.equalsIgnoreCase("actor")) {
            movie2.getActorsList().add(value);
        }
        if (element.equalsIgnoreCase("director")) {
            movie2.getDirectorsList().add(value);
        }
        if (element.equalsIgnoreCase("producer")) {
            movie2.getProducersList().add(value);
        }
        if (element.equalsIgnoreCase("writer")) {
            movie2.getWritersList().add(value);
        }
        if (element.equalsIgnoreCase("studio")) {
            movie2.setStudio(value);
        }
        if (element.equalsIgnoreCase("image")) {
            movie2.setImage(value);
        }
    }

    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        value = new String(ac, i, j);
    }

    public Movie loadBean(int movieListIndex) {
        movie.setTitle(movieList.get(movieListIndex).getTitle());
        movie.setImage(movieList.get(movieListIndex).getImage());
        movie.setRate(movieList.get(movieListIndex).getRate());
        for (int ii = 0; ii < movieList.get(movieListIndex).getTimesList().size(); ii++) {
            movie.getTimesList().add(movieList.get(movieListIndex).getTimesList().get(ii));
        }
        movie.setReleaseDate(movieList.get(movieListIndex).getReleaseDate());
        movie.setRunTime(movieList.get(movieListIndex).getRunTime());
        movie.setGenre(movieList.get(movieListIndex).getGenre());
        for (int ii = 0; ii < movieList.get(movieListIndex).getActorsList().size(); ii++) {
            movie.getActorsList().add(movieList.get(movieListIndex).getActorsList().get(ii));
        }
        for (int ii = 0; ii < movieList.get(movieListIndex).getDirectorsList().size(); ii++) {
            movie.getDirectorsList().add(movieList.get(movieListIndex).getDirectorsList().get(ii));
        }
        for (int ii = 0; ii < movieList.get(movieListIndex).getProducersList().size(); ii++) {
            movie.getProducersList().add(movieList.get(movieListIndex).getProducersList().get(ii));
        }
        for (int ii = 0; ii < movieList.get(movieListIndex).getWritersList().size(); ii++) {
            movie.getWritersList().add(movieList.get(movieListIndex).getWritersList().get(ii));
        }
        movie.setStudio(movieList.get(movieListIndex).getStudio());
        return movie;
    }

    public String getMessage() {
        return message;
    }

    public boolean findMovieByTitle(String query) {
        parseDocument();
        for (int i = 0; i < movieList.size(); i++) {
            if (movieList.get(i).getTitle().equalsIgnoreCase(query)) {
                loadBean(i);
                return true;
            }
        }
        return false;
    }

    public boolean findMovieByActorName(String query) {
        parseDocument();
        for (int i = 0; i < movieList.size(); i++) {
            for (int actor = 0; actor < movieList.get(i).getActorsList().size(); actor++) {
                if (movieList.get(i).getActorsList().get(actor).equalsIgnoreCase(query)) {
                    loadBean(i);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean findMovieByDirectorName(String query) {
        parseDocument();
        for (int i = 0; i < movieList.size(); i++) {
            for (int director = 0; director < movieList.get(i).getDirectorsList().size(); director++) {
                if (movieList.get(i).getDirectorsList().get(director).equalsIgnoreCase(query)) {
                    loadBean(i);
                    return true;
                }
            }

        }
        return false;
    }

//    public static void main(String[] args) {
//        StringBuilder xmlString = new StringBuilder();
//        Movie movie = new Movie();
//
//        SAXParseXML one = new SAXParseXML("movies.xml", movie, "click");
//        one.parseDocument();
//
//        for (int i = 0; i < one.movieList.size(); i++) {
//            if (one.movieList.get(i).getTitle().equalsIgnoreCase("krrish")) {
//                System.out.println(one.movieList.get(i).toString());
//                System.out.println(one.movieList.get(i).getTimesList().size());
//
//                for (int ii = 0; ii < one.movieList.get(i).getTimesList().size(); ii++) {
//                    List love = new ArrayList();
//                    love.add(one.movieList.get(i).getTimesList().get(ii));
//
//                    System.out.println(love.iterator().next());
//                }
//
//            }
//        }
//
//    }
}
