/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifg.backingbean;

import com.danielifg.bean.Movie;
import com.danielifg.dom.DOMParseXML;
import com.danielifg.sax.SAXParseXML;
import com.danielifg.stax.STAXParseXML;
import java.io.FileNotFoundException;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Document;

/**
 *
 * @author Daniel
 *
 * Class Controller to render the XML result from the DOM API parser
 */
@Named("BackingBean")
@RequestScoped
public class MoviesBackingBean implements Serializable {

    @Inject
    private Movie movie;

    private final Document doc = null;
    private String query;
    private String engine;
    private String message;
    private final String error;

    public MoviesBackingBean() {
        this.error = "Movie not available";
    }

    private static final String MOVIESXML = FacesContext.getCurrentInstance()
            .getExternalContext()
            .getRealPath("/resources/xml/movies.xml");
     private static final String MOVIESXSD = FacesContext.getCurrentInstance()
            .getExternalContext()
            .getRealPath("/resources/xml/moviesSchema.xsd");

    public void displayMovie() throws XMLStreamException, FileNotFoundException {


        switch (engine) {
            case "DOM":
                DOMParseXML dom = new DOMParseXML( movie,MOVIESXML,MOVIESXSD);
                if (!dom.findMovieByTitle(query)
                        && !dom.findMovieByActorName(query)
                        && !dom.findMovieByDirectorName(query)) {
                    this.setMessage(error);
                }
                break;

            case "SAX":
                SAXParseXML sax = new SAXParseXML(MOVIESXML, movie);
                if (!sax.findMovieByTitle(query)
                        && !sax.findMovieByActorName(query)
                        && !sax.findMovieByDirectorName(query)) {
                    this.setMessage(error);
                }
                break;

            case "StAX":
                STAXParseXML stax = new STAXParseXML(MOVIESXML, movie);
                if (!stax.findMovieByTitle(query)
                        && !stax.findMovieByActorName(query)
                        && !stax.findMovieByDirectorName(query)) {
                    this.setMessage(error);
                }
                break;
           
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
}
